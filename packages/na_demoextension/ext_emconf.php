<?php

$EM_CONF['na_demoextension'] = [
    'title' => 'Naderios YouTube Demo',
    'description' => 'YouTube Demo Extension',
    'category' => 'templates',
    'author' => 'Thomas Anders',
    'author_email' => 'me@naderio.de',
    'state' => 'alpha',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
